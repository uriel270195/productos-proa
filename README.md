### <center><font color=purple> Microservicio Productos Proa</font> </center>

## Requisitos
* Maven
* Java JDK 17

Para correr el proyecto, es necesario posicionarse en el directorio raíz del proyecto y ejecutar el siguiente comando:
* mvn clean install

Una vez terminado de compilar y generar el jar, posicionarse en la carpeta target, y ejecutar el siguiente comando:
* java -jar productos-0.0.1.jar

Se levantará el microservicio en ambiente local al puerto 8080 listo para usarse.

## URL contrato del servicio

|Ambiente|Método|URL|
|--|--|--|
|DEV|POST|http://localhost:8080/productos/
|DEV|PUT|http://localhost:8080/productos/{id}
|DEV|DELETE|http://localhost:8080/productos/{id}
|DEV|GET|http://localhost:8080/productos/brand/{brand}


### Métodos del servicio

|Nombre|Definición|
|--|--|
|createProduct|Método para crear un producto|
|updateProduct|Método para actualizar un producto|
|deleteProduct|Método para eliminar un producto|
|getProductsByBrand|Método para obtener una lista de productos buscando por el nombre de su marca|

<br>
<hr>

##### **Esquema de Seguridad**

|Elemento|Tipo|Obligatorio|Descripción|
|--|--|--|--|
|ID_CLIENT_SESSION|Header|SI|Identificador de Usuario que realiza el consumo. Solo aplica para el metodo getProductsByBrand|


<br>
<hr>

##### **Datos de entrada Generales**

|Elemento|Tipo|Obligatorio|Lista de valores|Descripción|
|--|--|--|--|--|
|header.ID_CLIENT_SESSION|Number|SI|78965088|Aplica solo a getProductsByBrand|

##### **Datos de salida Generales**

|Elemento|Tipo|Obligatorio en respuesta|Lista de valores|Comentarios|
|--|--|--|--|--|
|code|String|SI||0000 si la petición se atendió satisfactoriamente. Un valor distinto indica error al atender la petición o el código de rechazo.|
|message|String|SI||Mensaje descriptivo de la petición|
|body|Object|SI||contenido de la respuesta de la petición|

<br>
<hr>

#### Códigos de error
| Código| Descripción|
|--|--|
|000|Operación Exitosa |
|001|Una campo vacio|
|002|valida que no sea una cadena con un tamaño mayor o menor al maximo o minimo de caracteres establecido|
|004|valida que no sea un número mayor a lo establecido|
|005|valida que no sea un número menor a lo establecido.|
|401|No fue posible atender la solicitud, valor del ID_CLIENT_SESSION no es valido |
