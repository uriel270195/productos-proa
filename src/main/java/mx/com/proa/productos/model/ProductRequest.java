package mx.com.proa.productos.model;

public class ProductRequest {
    private String name;
    private String brand;
    private Double price;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "{" +
                " name='" + getName() + "'" +
                ", brand='" + getBrand() + "'" +
                ", price='" + getPrice() + "'" +
                "}";
    }

}
