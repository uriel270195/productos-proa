package mx.com.proa.productos.model;

public class BodyResponse<k> {
    private String code;
    private String message;
    private k body;

    public BodyResponse(){
        this.code = "000";
        this.message = "Solicitud ejecutada correctamente";
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public k getBody() {
        return this.body;
    }

    public void setBody(k body) {
        this.body = body;
    }


    @Override
    public String toString() {
        return "{" +
            " code='" + getCode() + "'" +
            ", message='" + getMessage() + "'" +
            ", body='" + getBody() + "'" +
            "}";
    }

}
