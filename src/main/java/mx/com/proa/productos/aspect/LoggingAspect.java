package mx.com.proa.productos.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
    @Before("execution(* mx.com.proa.productos.controller.ProductController.*(..))")
    public void logRequest(JoinPoint joinPoint) {
        System.out.println();
        System.out.println("Request method: " + joinPoint.getSignature().getName());
        System.out.println("Request arguments:");
        for (Object i : joinPoint.getArgs()) {
            System.out.println("\t Type: " + i.getClass());
            System.out.println("\t Value: " + i.toString() + "\n");
        }
    }
    
    @AfterReturning(pointcut = "execution(* mx.com.proa.productos.controller.ProductController.*(..))", returning = "result")
    public void logResponse(JoinPoint joinPoint, Object result) {
        System.out.println("Response: ");
        System.out.println("\t Value: " + result.toString());
        System.out.println();
    }
}
