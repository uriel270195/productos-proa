package mx.com.proa.productos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.proa.productos.entity.ProductEntity;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    List<ProductEntity> findByBrand(String brand);
}