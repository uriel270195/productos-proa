package mx.com.proa.productos.service;

import mx.com.proa.productos.model.BodyResponse;
import mx.com.proa.productos.model.ProductRequest;
import mx.com.proa.productos.model.ProductResponse;

public interface ProductService {
    public BodyResponse<ProductResponse> createProduct(ProductRequest request);

    public BodyResponse<ProductResponse> updateProduct(Long id, ProductRequest request);

    public BodyResponse<ProductResponse> deleteProduct(Long id);

    public BodyResponse<?> getProductsByBrand(String sessionId, String brand);
}
