package mx.com.proa.productos.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.proa.productos.entity.ProductEntity;
import mx.com.proa.productos.exception.RequestException;
import mx.com.proa.productos.model.BodyResponse;
import mx.com.proa.productos.model.ProductRequest;
import mx.com.proa.productos.model.ProductResponse;
import mx.com.proa.productos.repository.ProductRepository;
import mx.com.proa.productos.service.ProductService;
import mx.com.proa.productos.util.Utils;
import mx.com.proa.productos.util.ValidatorsUtils;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ValidatorsUtils validatorsUtils;

    @Override
    public BodyResponse<ProductResponse> createProduct(ProductRequest request) {
        BodyResponse<ProductResponse> response = new BodyResponse<>();
        ProductEntity productEntity = Utils.mappingObject(request, ProductEntity.class);

        try {
            validatorsUtils.validateString(request.getName(), "name").noEmpty().needToBeLessThan(31).needToBeMoreThan(4)
                    .validateString(request.getBrand(), "brand").noEmpty().needToBeLessThan(31).needToBeMoreThan(4)
                    .validateDouble(request.getPrice(), "price").noEmpty().needToBeMoreThan(0).needToBeLessThan(10001);
        } catch (RequestException e) {
            response.setCode(e.getCode());
            response.setMessage(e.getMessage());
            return response;
        }

        ProductResponse responseBody = Utils.mappingObject(productRepository.save(productEntity),
                ProductResponse.class);

        response.setBody(responseBody);

        return response;
    }

    @Override
    public BodyResponse<ProductResponse> updateProduct(Long id, ProductRequest request) {
        BodyResponse<ProductResponse> response = new BodyResponse<>();
        ProductEntity product;

        try {
            product = productRepository.findById(id).orElseThrow();

            validatorsUtils.validateString(request.getName(), "name").noEmpty().needToBeLessThan(31).needToBeMoreThan(4)
                    .validateString(request.getBrand(), "brand").noEmpty().needToBeLessThan(31).needToBeMoreThan(4)
                    .validateDouble(request.getPrice(), "price").noEmpty().needToBeMoreThan(0).needToBeLessThan(10001);
        } catch (RequestException e) {
            response.setCode(e.getCode());
            response.setMessage(e.getMessage());
            return response;
        } catch (NoSuchElementException e) {
            response.setCode("005");
            response.setMessage("no encontro el producto con el id establecido");
            return response;
        }

        product.setName(request.getName());
        product.setBrand(request.getBrand());
        product.setPrice(request.getPrice());

        ProductResponse responseBody = Utils.mappingObject(productRepository.save(product), ProductResponse.class);

        response.setBody(responseBody);

        return response;
    }

    @Override
    public BodyResponse<ProductResponse> deleteProduct(Long id) {
        BodyResponse<ProductResponse> response = new BodyResponse<>();
        ProductEntity product;

        try {
            product = productRepository.findById(id).orElseThrow();
        } catch (NoSuchElementException e) {
            response.setCode("005");
            response.setMessage("no encontro el producto con el id establecido");
            return response;
        }

        productRepository.delete(product);

        ProductResponse responseBody = Utils.mappingObject(product, ProductResponse.class);

        response.setBody(responseBody);

        return response;
    }

    @Override
    public BodyResponse<?> getProductsByBrand(String sessionId, String brand) {
        BodyResponse<List<ProductResponse>> response = new BodyResponse<>();
        List<ProductResponse> productsResp = new ArrayList<>();

        if (!"78965088".equals(sessionId)) {
            response.setCode("401");
            response.setMessage("Sesión no válida");
            return response;
        }

        List<ProductEntity> products = productRepository.findByBrand(brand);

        for (ProductEntity i : products) {
            productsResp.add(Utils.mappingObject(i, ProductResponse.class));
        }

        response.setBody(productsResp);

        return response;
    }

}
