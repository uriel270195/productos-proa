package mx.com.proa.productos.util;

import org.springframework.stereotype.Component;

import mx.com.proa.productos.exception.RequestException;

@Component
public class ValidatorsUtils {
    public static interface ENUM_TYPES {
        String STRING = "STRING";
        String INTEGER = "INTEGER";
        String LONG = "LONG";
        String DOUBLE = "DOUBLE";
    }

    private String valueString;
    private Integer valueInt;
    private Long valueLong;
    private Double valueDouble;
    private String type;
    private String ObjectName;

    public ValidatorsUtils validateString(String value, String ObjectName) {
        this.valueString = value;
        this.type = ENUM_TYPES.STRING;
        this.ObjectName = ObjectName;
        return this;
    }

    public ValidatorsUtils validateInteger(int value, String ObjectName) {
        this.valueInt = value;
        this.type = ENUM_TYPES.INTEGER;
        this.ObjectName = ObjectName;
        return this;
    }

    public ValidatorsUtils validateLong(long value, String ObjectName) {
        this.valueLong = value;
        this.type = ENUM_TYPES.LONG;
        this.ObjectName = ObjectName;
        return this;
    }
    public ValidatorsUtils validateDouble(Double value, String ObjectName) {
        this.valueDouble = value;
        this.type = ENUM_TYPES.DOUBLE;
        this.ObjectName = ObjectName;
        return this;
    }

    public ValidatorsUtils noEmpty() {
        switch (this.type) {
            case ENUM_TYPES.STRING:
                if (this.valueString == null || this.valueString.equals("")) {
                    throw new RequestException("001", "El campo " + ObjectName + " esta vacio");
                }
                break;
            case ENUM_TYPES.INTEGER:
                if (this.valueInt == null) {
                    throw new RequestException("001", "El campo " + ObjectName + " esta vacio");
                }
                break;
            case ENUM_TYPES.LONG:
                if (this.valueLong == null) {
                    throw new RequestException("001", "El campo " + ObjectName + " esta vacio");
                }
                break;
            case ENUM_TYPES.DOUBLE:
                if (this.valueDouble == null) {
                    throw new RequestException("001", "El campo " + ObjectName + " esta vacio");
                }
                break;
        }
        return this;
    }

    public ValidatorsUtils needToBeMoreThan(Integer number) {
        switch (this.type) {
            case ENUM_TYPES.STRING:
                if (this.valueString.length() <= number) {
                    throw new RequestException("002",
                            "El campo " + ObjectName + " debe contener mas de " + number + " caracteres");
                }
                break;
            case ENUM_TYPES.INTEGER:
                if (this.valueInt <= number) {
                    throw new RequestException("003", "El campo " + ObjectName + " debe ser mayor a " + number);
                }
                break;
            case ENUM_TYPES.DOUBLE:
                if (this.valueDouble <= number) {
                    throw new RequestException("003", "El campo " + ObjectName + " debe ser mayor a " + number);
                }
                break;
        }
        return this;
    }

    public ValidatorsUtils needToBeLessThan(Integer number) {
        switch (this.type) {
            case ENUM_TYPES.STRING:
                if (this.valueString.length() >= number) {
                    throw new RequestException("002",
                            "El campo " + ObjectName + " debe contener menos de " + number + " caracteres");
                }
                break;
            case ENUM_TYPES.INTEGER:
                if (this.valueInt >= number) {
                    throw new RequestException("004", "El campo " + ObjectName + " debe ser menor que " + number);
                }
                break;
            case ENUM_TYPES.DOUBLE:
                if (this.valueDouble >= number) {
                    throw new RequestException("004", "El campo " + ObjectName + " debe ser menor que " + number);
                }
                break;
        }
        return this;
    }
}