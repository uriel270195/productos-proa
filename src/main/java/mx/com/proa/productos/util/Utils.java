package mx.com.proa.productos.util;

import java.lang.reflect.Field;

public class Utils {

    public static <K, T> T mappingObject(K source, Class<T> targetClass) {
        try {
            // Crear una nueva instancia de T
            T target = targetClass.getDeclaredConstructor().newInstance();

            // Obtener todos los campos de la clase fuente
            Field[] sourceFields = source.getClass().getDeclaredFields();

            for (Field sourceField : sourceFields) {
                sourceField.setAccessible(true); // Asegurar el acceso a campos privados
                Object value = sourceField.get(source); // Obtener el valor del campo de la instancia fuente

                try {
                    // Intentar encontrar el campo correspondiente en la clase destino
                    Field targetField = targetClass.getDeclaredField(sourceField.getName());
                    targetField.setAccessible(true); // Asegurar el acceso a campos privados
                    targetField.set(target, value); // Establecer el valor del campo en la instancia destino
                } catch (NoSuchFieldException e) {
                    // Si el campo no existe en la clase destino, simplemente lo omitimos
                }
            }

            return target;
        } catch (Exception e) {
            e.printStackTrace();
            return null; // Manejar excepciones apropiadamente en un caso real
        }
    }

}
