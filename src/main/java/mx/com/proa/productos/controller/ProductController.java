package mx.com.proa.productos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.proa.productos.model.BodyResponse;
import mx.com.proa.productos.model.ProductRequest;
import mx.com.proa.productos.model.ProductResponse;
import mx.com.proa.productos.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseEntity<BodyResponse<ProductResponse>> createProduct(@RequestBody ProductRequest request) {
        BodyResponse<ProductResponse> resp = productService.createProduct(request);

        if (resp.getCode().equals("000")) {
            return ResponseEntity.ok(resp);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BodyResponse<ProductResponse>> updateProduct(@PathVariable Long id, @RequestBody ProductRequest request) {
        BodyResponse<ProductResponse> resp = productService.updateProduct(id, request);

        if (resp.getCode().equals("000")) {
            return ResponseEntity.ok(resp);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BodyResponse<ProductResponse>> deleteProduct(@PathVariable Long id) {
        BodyResponse<ProductResponse> resp = productService.deleteProduct(id);

        if (resp.getCode().equals("000")) {
            return ResponseEntity.ok(resp);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp);
    }

    @GetMapping("/brand/{brand}")
    public ResponseEntity<BodyResponse<?>> getProductsByBrand(@RequestHeader("ID_CLIENT_SESSION") String sessionId,
            @PathVariable String brand) {
        BodyResponse<?> resp = productService.getProductsByBrand(sessionId, brand);

        if (resp.getCode().equals("000")) {
            return ResponseEntity.ok(resp);
        } else if (resp.getCode().equals("401")) {
            return ResponseEntity.status(401).body(resp);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp);
    }
}
